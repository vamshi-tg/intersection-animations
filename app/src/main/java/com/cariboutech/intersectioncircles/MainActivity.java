package com.cariboutech.intersectioncircles;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {

    LinearLayout layout3;
    LinearLayout layout2;
    LinearLayout layout1;


    public void animateCircles(View view){
        Log.i("Info","Animating");

//        layout3.animate().translationXBy(-170f).translationYBy(-170f).setDuration(500);
//
//        layout2.animate().setStartDelay(500).translationXBy(-340f).translationYBy(-340f).setDuration(500);
//
//        layout1.animate().setStartDelay(1000).translationXBy(-600f).translationYBy(-600f).setDuration(500);

            circlesBeforeEnterAnimation();

            circlesAfterEnterAnimation();
 }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        layout1 = findViewById(R.id.outer_circle);
        layout2 = findViewById(R.id.center_circle);
        layout3 = findViewById(R.id.inner_circle);

    }

    @Override
    protected void onStart() {
        super.onStart();

        circlesBeforeEnterAnimation();

        circlesAfterEnterAnimation();

        final Handler handler = new Handler();

        Runnable run = new Runnable() {
            @Override
            public void run() {
                circlesBeforeEnterAnimation();

                circlesAfterEnterAnimation();

                handler.postDelayed(this, 1800);
            }
        };

        handler.post(run);

    }

    private void circlesBeforeEnterAnimation(){
        layout3.setTranslationX(170f);
        layout3.setTranslationY(170f);

        layout2.setTranslationX(340f);
        layout2.setTranslationY(340f);

        layout1.setTranslationX(600f);
        layout1.setTranslationY(600f);
    }

    private void circlesAfterEnterAnimation(){
        layout3.animate().translationXBy(-170f).translationYBy(-170f).setDuration(300);

        layout2.animate().setStartDelay(300).translationXBy(-340f).translationYBy(-340f).setDuration(500);

        layout1.animate().setStartDelay(800).translationXBy(-600f).translationYBy(-600f).setDuration(600);
    }



}
